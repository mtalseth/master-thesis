source('RelCompPlot.R')
library(pls)
library(R.matlab)
library(xtable)

myfile<-readMat('corn.mat')
myspectra<-myfile$m5spec$data
colnames(myspectra)<-myfile$m5spec$axisscale[[2]][[1]]
myprops<-myfile$propvals$data
colnames(myprops)<-myfile$propvals$label[[2]][[1]]

XX<-myspectra
YY<-myprops[,c(1,2)]
mydata<-data.frame(cbind(YY,XX))
colnames(mydata)<-c('Moisture', 'Oil',colnames(myspectra))

#The distribution
var(YY)
cor(YY)

YY.center<-scale(YY, scale=F)
XX.center<-scale(XX, scale=F)
par(mfrow=c(2,2))
for(resp in 1:dim(YY.center)[2]){
  plotprops(YY.center[,resp],XX.center, ncomp=15)
  title(colnames(YY.center)[resp])
}
#Since p>n we can not find conditional correlation as with the pig data...

#split up in testset and training data
i.test<- sample(1:80, 40, replace=F)
test.data<-mydata[i.test,]
train.data<- mydata[-i.test,]

#Centering all data with means of training data
pigmeans<-colMeans(train.data)
train.data<-train.data - matrix(rep(pigmeans, dim(train.data)[1]), byrow=T, ncol=length(pigmeans))
test.data<- test.data - matrix(rep(pigmeans, dim(test.data)[1]), byrow=T, ncol=length(pigmeans))


#Fitting PLSR-models
ncomp<-39
multi<-plsr(matrix(c(Moisture,Oil), ncol=2, byrow=F)~., data=train.data, ncomp=ncomp)
uniprot<-plsr(matrix(c(Moisture,Oil), ncol=2, byrow=F)[,1]~., data=train.data, ncomp=ncomp)
unistarch<-plsr(matrix(c(Moisture,Oil), ncol=2, byrow=F)[,2]~., data=train.data, ncomp=ncomp)

#Prediction error
errmulti<- MSEP(multi, newdata=test.data, estimate=c('test'))
err.m.prot<- errmulti$val[,1,]
err.u.prot<- MSEP(uniprot, newdata=test.data, estimate=c('test'))$val[,1,]
err.m.starch<- errmulti$val[,2,]
err.u.starch<- MSEP(unistarch, newdata=test.data, estimate=c('test'))$val[,1,]

#Plotting prediction error
par(mfcol=c(1,3))
plot(1:length(err.m.prot)-1, err.m.prot,type='l',ylim=c(min(err.m.prot,err.u.prot),max(err.m.prot,err.u.prot)), lty=2, col='black', xlab='component', ylab='MSEP')
lines(1:length(err.u.prot) -1, err.u.prot, col='green')
title('Moisture')
par(xpd=T)
legend("topright", inset=c(0,0),legend=c('PLS1', 'PLS2'),col=c('green','black'), lty=c(1,2))
par(xpd=F)

plot(1:length(err.m.starch)-1, err.m.starch,type='l',ylim=c(min(err.m.starch, err.u.starch), max(err.m.starch, err.u.starch)) ,lty=2, col='red', xlab='component', ylab='MSEP')
lines(1:length(err.u.starch)-1, err.u.starch,col='blue')
title('Oil')
par(xpd=T)
legend("topright", inset=c(0,0),legend=c('PLS1', 'PLS2'),col=c('blue','red'), lty=c(1,2))
par(xpd=F)

plot(1:length(err.m.prot)-1, err.m.prot,type='l', lty=2, ylim=c(min(err.u.starch,err.m.starch,err.u.prot,err.m.prot),max(err.u.starch,err.m.starch,err.u.prot,err.m.prot)), col='black', xlab='component', ylab='MSEP')
lines(1:length(err.u.prot)-1, err.u.prot, col='green')
lines(1:length(err.m.starch)-1, err.m.starch,type='l', lty=2, col='red')
lines(1:length(err.u.starch)-1, err.u.starch,col='blue')

#looking at betas
moduni1<-uniprot
moduni2<-unistarch
modmulti<-multi
p<-dim(XX)[2]
maxcomp<-ncomp

#looking at some betas
coff_c1_y1<-c()
coff_c2_y1<-c()
c1_y1_conf_low<-c()
c1_y1_conf_high<-c()
c2_y1_conf_low<-c()
c2_y1_conf_high<-c()

coff_c1_y2<-c()
coff_c2_y2<-c()
c1_y2_conf_low<-c()
c1_y2_conf_high<-c()
c2_y2_conf_low<-c()
c2_y2_conf_high<-c()

for(comp in 1:maxcomp){
  est.betauni1 <- moduni1$coefficients[1:p,1,comp]
  est.betauni2 <- moduni2$coefficients[1:p,1,comp]
  Xdata<-matrix(c(est.betauni1, est.betauni2), ncol=2, byrow=F)
  est.betamulti1 <- modmulti$coefficients[1:p,1,comp]
  est.betamulti2 <- modmulti$coefficients[1:p,2,comp]
  mymod1<- lm(est.betamulti1~Xdata)
  c1_y1_conf_low[comp]<-confint(mymod1, 2)[1]
  c1_y1_conf_high[comp]<-confint(mymod1, 2)[2]
  c2_y1_conf_low[comp]<-confint(mymod1, 3)[1]
  c2_y1_conf_high[comp]<-confint(mymod1, 3)[2]
  coff_c1_y1[comp]<-mymod1$coefficient[2]#for y1 beta
  coff_c2_y1[comp]<-mymod1$coefficient[3]
  
  mymod2<- lm(est.betamulti2~Xdata)
  coff_c1_y2[comp]<-mymod2$coefficient[2]#for y1 beta
  coff_c2_y2[comp]<-mymod2$coefficient[3]
  c1_y2_conf_low[comp]<-confint(mymod2, 2)[1]
  c1_y2_conf_high[comp]<-confint(mymod2, 2)[2]
  c2_y2_conf_low[comp]<-confint(mymod2, 3)[1]
  c2_y2_conf_high[comp]<-confint(mymod2, 3)[2]
}
par(mfrow=c(2,1), oma=c(3,0,0,0), mar=c(1,1,1,1) + 1.5)
plot(1:maxcomp, coff_c1_y1,type='b',pch=16, ylim=c(-0.5,1.5), col='green', xlab='', ylab='')
arrows(1:maxcomp, coff_c1_y1,1:maxcomp,c1_y1_conf_low, angle=90, length=0.2, col='green', lty=2)
arrows(1:maxcomp, coff_c1_y1,1:maxcomp,c1_y1_conf_high, angle=90, length=0.2, col='green', lty=2)
mtext("Number of components",side=1, line=2)

lines(1:maxcomp,type='b',pch=16, coff_c2_y1)
arrows(1:maxcomp, coff_c2_y1,1:maxcomp,c2_y1_conf_low, angle=90, length=0.2, lty=2)
arrows(1:maxcomp, coff_c2_y1,1:maxcomp,c2_y1_conf_high, angle=90, length=0.2, lty=2)
title('a)', adj=0)

plot(1:maxcomp, coff_c1_y2,type='b',pch=16, ylim=c(-0.5,1.5), col='green', xlab='', ylab='')
arrows(1:maxcomp, coff_c1_y2,1:maxcomp,c1_y2_conf_low, angle=90, length=0.2, col='green', lty=2)
arrows(1:maxcomp, coff_c1_y2,1:maxcomp,c1_y2_conf_high, angle=90, length=0.2, col='green', lty=2)
mtext("Number of components",side=1, line=2)

lines(1:maxcomp,type='b',pch=16, coff_c2_y2)
arrows(1:maxcomp, coff_c2_y2,1:maxcomp,c2_y2_conf_low, angle=90, length=0.2, lty=2)
arrows(1:maxcomp, coff_c2_y2,1:maxcomp,c2_y2_conf_high, angle=90, length=0.2, lty=2)
title('b)', adj=0)

par(xpd=NA)
legend(10,-2,ncol=2,legend=c('PLS1, Moisture', 'PLS1, Oil'),col=c('green','black'), pch=16, lty=1)
par(xpd=F)

#Loading weights
coff_c1_y2<-c()
coff_c2_y2<-c()
c1_y2_conf_low<-c()
c1_y2_conf_high<-c()
c2_y2_conf_low<-c()
c2_y2_conf_high<-c()

for(comp in 1:maxcomp){
  loaduni1<-(moduni1$loading.weights[,comp])
  loaduni2<-(moduni2$loading.weights[,comp])
  loadmulti<-(modmulti$loading.weights[,comp])
  Xdata<-matrix(c(loaduni1, loaduni2), ncol=2, byrow=F)
  mymod<-lm(loadmulti~Xdata)
  coff_c1_y2[comp]<-mymod$coefficient[2]#for y1 beta
  coff_c2_y2[comp]<-mymod$coefficient[3]
  c1_y2_conf_low[comp]<-confint(mymod, 2)[1]
  c1_y2_conf_high[comp]<-confint(mymod, 2)[2]
  c2_y2_conf_low[comp]<-confint(mymod, 3)[1]
  c2_y2_conf_high[comp]<-confint(mymod, 3)[2]
}
plot(1:maxcomp, coff_c1_y2,type='b',pch=16, ylim=c(-2,2), col='green')
arrows(1:maxcomp, coff_c1_y2,1:maxcomp,c1_y2_conf_low, angle=90, length=0.2, col='green', lty=2)
arrows(1:maxcomp, coff_c1_y2,1:maxcomp,c1_y2_conf_high, angle=90, length=0.2, col='green', lty=2)

lines(1:maxcomp, (coff_c2_y2), type='b',pch=16)
arrows(1:maxcomp, (coff_c2_y2) ,1:maxcomp,c2_y2_conf_low, angle=90, length=0.2, lty=2)
arrows(1:maxcomp, (coff_c2_y2) ,1:maxcomp,c2_y2_conf_high, angle=90, length=0.2, lty=2)
title('Loading Weights')

#looking at loadings
maxcomp<-6
par(mfrow=c(2,maxcomp/2), oma=c(5,0,0,0))
#plot(1:p,rep(NA,p), ylim=c(-1,1))
for(comp in 1:maxcomp){
  plot(1:p, modmulti$loading.weights[,comp],type='l', col='red',ylim=c(-1,1))
  if(coff_c1_y2[comp]<0){
    moduni1$loading.weights[,comp]<- -moduni1$loading.weights[,comp]
  }
  if(coff_c2_y2[comp]<0){
    moduni2$loading.weights[,comp]<- -moduni2$loading.weights[,comp]
  }
  lines(1:p, moduni1$loading.weights[,comp],type='l', col='green',ylim=c(-1,1))
  #lines(1:p, -moduni1$loading.weights[,comp],type='l', col='red',ylim=c(-1,1))
  lines(1:p, moduni2$loading.weights[,comp],type='l', col='black',ylim=c(-1,1))
  #lines(1:p, -moduni2$loading.weights[,comp],type='l', col='black',ylim=c(-1,1))
  #lines(1:p, -modmulti$loading.weights[,comp],type='l', col='green',ylim=c(-1,1))
  title(comp)
}
par(xpd=NA)
legend(-9,-2,ncol=3,legend=c('PLS1, y1', 'PLS1, y2', 'PLS2'),col=c('green','black', 'red'), lty=1)
par(xpd=F)
